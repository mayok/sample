module.exports = (grunt) ->
  'use strict'
  pkg = grunt.file.readJSON 'package.json'

  grunt.initConfig
    cssmin:
      compress:
        files:
          './min.css': ['css/style.css']

    watch:
      files:"css/*.css",
      tasks:['cssmin']

    autoprefixer:
      options:
        browsers:['last 2 version']
      files:
        expand:true
        flatten:true
        src: 'css/*.css'
        dest: 'dest/css/'

    kss:
      options:
        includeType: 'css'
        includePath: 'css/style.css'
        template: 'doc/template'
      dist:
        files:
          'doc/':['css']

    sass:
      dist:
        files:
          'css/style.css':'sass/style.sass'

  grunt.loadNpmTasks 'grunt-kss'
  grunt.loadNpmTasks 'grunt-sass'
  grunt.loadNpmTasks 'grunt-autoprefixer'
  grunt.loadNpmTasks 'grunt-contrib-watch'
  grunt.loadNpmTasks 'grunt-contrib-cssmin'


  grunt.registerTask "default",["cssmin","watch"]
